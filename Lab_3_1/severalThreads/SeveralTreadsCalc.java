package severalThreads;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class SeveralTreadsCalc {
    long sumMas = 0;
    int numThread;
    int[] array;
    CyclicBarrier barrier;

    public SeveralTreadsCalc(int numThread, int[] array)
    {
        this.numThread = numThread;
        this.array = array;
        calcResult();
    }

    synchronized public void calcResult()
    {
        int[] beginMas = new int[numThread];
        int[] endMas = new int[numThread];
        for (int i = 0; i < numThread; i++)
            beginMas[i] = array.length / numThread * i;
        for (int i = 0; i < numThread - 1; i++)
            endMas[i] = beginMas[i + 1] - 1;

        endMas[numThread - 1] = array.length - 1;

        startTreads(numThread, array, beginMas, endMas);

        tryWait();
    }

    public void startTreads(int numThread, int[] mas, int[] beginMas, int[] endMas)
    {
        barrier = new CyclicBarrier(numThread, new Runnable() { public void run() { Notify(); } });
        Worker[] threadsOther = new Worker[numThread];
        Thread[] threads = new Thread[numThread];

        for (int i = 0; i < numThread; i++) {
            threadsOther[i] = new Worker(this, mas, beginMas[i], endMas[i]);
            threads[i] = new Thread(threadsOther[i]);
            threads[i].start();
        }
    }

    synchronized public void Notify() 
    {
        notify();
    }

    synchronized public void tryWait()
    {
        try {
            wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    
    synchronized public void setPartSum(long partSum)
    {
        sumMas = sumMas + partSum;
    }

    public void tryBarrierAwait()
    {
        try {
            barrier.await();
        } catch (InterruptedException ex) {
            return;
        } catch (BrokenBarrierException ex) {
            return;
        }
    }

    public long getSum()
    {
        return sumMas;
    }
}
