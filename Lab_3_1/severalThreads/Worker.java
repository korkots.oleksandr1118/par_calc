package severalThreads;

class Worker implements Runnable {

    public long partSum = 0;
    int[] mas;
    int begin, end;
    SeveralTreadsCalc calculate;

    Worker(SeveralTreadsCalc calculate, int[] curr_mas, int curr_begin, int curr_end) {
        mas = curr_mas;
        begin = curr_begin;
        end = curr_end;
        this.calculate = calculate;
    }

    @Override
    public void run() {
        for (int i = begin; i <= end; i++) {
            partSum = partSum + mas[i];
        }
        calculate.setPartSum(partSum);
        calculate.tryBarrierAwait();
    }
}

