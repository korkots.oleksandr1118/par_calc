package severalThreads;
public class SingleTreadCalc implements ThreadsCalc {
    int threadCount = 0;
    long sumMas = 0;
    SumThreadOther STO;
    int[] array;

    public SingleTreadCalc(int[] array)
    {
        this.array = array;
        STO = new SumThreadOther(this, array,  0, array.length - 1);
        calcResult();
    }

    public long GetSum()
    {
        return STO.partSum;
    }

    private void calcResult()
    {
        STO.run();
    }

    synchronized public void setPartSum(long partSum){
        sumMas = sumMas + partSum;
        threadCount++;
        notify();
    }
}
