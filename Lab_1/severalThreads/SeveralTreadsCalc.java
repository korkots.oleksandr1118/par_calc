package severalThreads;

public class SeveralTreadsCalc implements ThreadsCalc {
    long sumMas = 0;
    int threadCount = 0;
    int numThread;
    int[] array;

    public SeveralTreadsCalc(int numThread, int[] array)
    {
        this.numThread = numThread;
        this.array = array;
        calcResult();
    }

    synchronized public void calcResult()
    {
        int[] beginMas = new int[numThread];
        int[] endMas = new int[numThread];
        for (int i = 0; i < numThread; i++)
            beginMas[i] = array.length / numThread * i;
        for (int i = 0; i < numThread - 1; i++)
            endMas[i] = beginMas[i + 1] - 1;

        endMas[numThread - 1] = array.length - 1;

        startTreads(numThread, array, beginMas, endMas);

        waitTreads(numThread);
    }

    public void startTreads(int numThread, int[] mas, int[] beginMas, int[] endMas)
    {
        SumThreadOther[] threadsOther = new SumThreadOther[numThread];
        Thread[] threads = new Thread[numThread];

        for (int i = 0; i < numThread; i++) {
            threadsOther[i] = new SumThreadOther(this, mas, beginMas[i], endMas[i]);
            threads[i] = new Thread(threadsOther[i]);
            threads[i].start();
        }
    }

    public void waitTreads(int numThread)
    {
        while (threadCount < numThread){
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    synchronized public void setPartSum(long partSum){
        sumMas = sumMas + partSum;
        threadCount++;
        notify();
    }

    public long getSum()
    {
        return sumMas;
    }
}
