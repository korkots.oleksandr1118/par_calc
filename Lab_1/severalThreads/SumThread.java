package severalThreads;

class SumThread extends Thread {
    public long partSum = 0;
    int[] mas;
    int begin, end;

    SumThread(int[] curr_mas, int curr_begin, int curr_end) {
        mas = curr_mas;
        begin = curr_begin;
        end = curr_end;
    }

    public void run() {
        for (int i = begin; i <= end; i++) {
            partSum = partSum + mas[i];
        }
    }
}

