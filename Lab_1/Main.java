import severalThreads.SeveralTreadsCalc;
import severalThreads.SingleTreadCalc;

public class Main {
    public static void fillArray(int[] array)
    {
        for (int i = 0; i < array.length; i++)
            array[i] = i;
    }

    public static long sumArray(int[] array)
    {
        long sumMas = 0;
        for (int i = 0; i < array.length; i++) {
            sumMas += array[i];
        }
        return sumMas;
    }

    public static void main(String[] args) throws InterruptedException {
        int size = 1000000;
        int[] mas = new int[size];
        fillArray(mas);

        System.out.println("Сума в однопоточному режимі:");
        System.out.println(sumArray(mas));
        System.out.println(new SingleTreadCalc(mas).GetSum());

        System.out.println("Сума в багатопоточному режимі:");
        System.out.println(new SeveralTreadsCalc(3, mas).getSum());
    }
}