public class SeveralTredsCalc {
    public long[] array;
    public int threadsCount = 0;
    public int usefullArrayLength = 0;
    private int threadNum = 0;
    private SumThread[] treads;

    public SeveralTredsCalc(long[] array) {
        this.array = array;
        usefullArrayLength = array.length;
        threadsCount = usefullArrayLength / 2;
        calcResult();
    }

    synchronized public void calcResult()
    {
        startTreads(threadsCount, usefullArrayLength);
        
        while (usefullArrayLength > 1) {
            threadNum = 0;
            notifyTraeds(threadsCount, usefullArrayLength);
            waitMainTread(threadsCount);
            usefullArrayLength = usefullArrayLength / 2 + usefullArrayLength % 2;
            threadsCount = usefullArrayLength / 2;
        }
    }

    private void startTreads(int threadsCount, int usefullArrayLength)
    {
        treads = new SumThread[threadsCount];

        for (int i = 0; i < threadsCount; i++) {
            treads[i] = new SumThread(this, i);
            treads[i].start();
        }
    }

    private void waitMainTread(int threadsCount)
    {
        while (threadsCount > threadNum){
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void notifyTraeds(int threadsCount, int usefullArrayLength) {
        for (int i = 0; i < threadsCount; i++) {
            treads[i].Notify();
        }
    }

    synchronized public void PartSum(int index, long sum) {
        array[index] = sum;
        threadNum++;
        notify();
    }

    public long GetSum() {
        return array[0];
    }
}