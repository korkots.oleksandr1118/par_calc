class SumThread extends Thread {
    int index;
    long partSum = 0;
    SeveralTredsCalc treadCalc;

    SumThread(SeveralTredsCalc treadCalc, int index) {
        this.index = index;
        this.treadCalc = treadCalc;
    }

    public void run() {
        SumElements();
    }

    private void SumElements() {
        int endIndex = treadCalc.usefullArrayLength - 1 - index;
        long sum = endIndex != index ? treadCalc.array[index] + treadCalc.array[endIndex] : treadCalc.array[index];
        treadCalc.PartSum(index, sum);

        if (index >= (treadCalc.usefullArrayLength / 2 + treadCalc.usefullArrayLength % 2) / 2) return;
        waitTread();
        SumElements();
    }

    private synchronized void waitTread() {
        try {
            wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized void Notify() {
        notify();
    }
}

