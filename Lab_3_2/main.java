class Main {
    public static void main(String[] args) {
        Main main = new Main();
        main.starter(250);
    }

    public void starter(int size) {
        long SumMas = 0;

        long[] mas = new long[size];

        for (int i = 0; i < size; i++)
            mas[i] = i;

        for (int i = 0; i < size; i++)
            SumMas = SumMas + mas[i];

        System.out.println("Сума в однопоточному:");
        System.out.println(SumMas);

        System.out.println("Сума в багатопоточному:");
        System.out.println(new SeveralTredsCalc(mas).GetSum());
    }
}