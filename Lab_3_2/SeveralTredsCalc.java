import java.util.concurrent.Phaser;

public class SeveralTredsCalc {
    public long[] array;
    public int threadsCount = 0;
    public int usefullArrayLength = 0;
    public Phaser phaser;
    private SumThread[] treads;

    public SeveralTredsCalc(long[] array) {
        this.array = array;
        usefullArrayLength = array.length;
        threadsCount = usefullArrayLength / 2;
        calcResult();
    }

    synchronized public void calcResult()
    {
        startTreads();
        
        while (usefullArrayLength > 1) {
            tryWaitMainTread();
        }
    }

    private void startTreads()
    {
        initPhaser();
        treads = new SumThread[threadsCount];

        for (int i = 0; i < threadsCount; i++) {
            treads[i] = new SumThread(this, i);
            treads[i].start();
        }
    }

    private void initPhaser()
    {
        phaser = new Phaser(threadsCount) 
        { 
            protected boolean onAdvance(int phase, int registeredParties) 
            { 
                usefullArrayLength = usefullArrayLength / 2 + usefullArrayLength % 2;
                threadsCount = usefullArrayLength / 2;
                notifyMainTread();
                return super.onAdvance(phase, registeredParties); 
            }
        };
    }

    synchronized private void tryWaitMainTread()
    {
        try {
            wait();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    synchronized private void notifyMainTread()
    {
        notify();
    }

    public void PartSum(int index, long sum) 
    {
        array[index] = sum;
    }

    public long GetSum() {
        return array[0];
    }
}